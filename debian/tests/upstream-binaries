#!/bin/sh
# autopkgtest driver for ROCm upstream binaries
#
# Expects argument "$1" to be the name of a sub-directory, such that the
# following following directory exists:
#
#   /usr/libexec/rocm/$1
#
# Will run all executables in that directory, and exit with status=1 if
# any failure occurred, otherwise with status=0. A failure is defined as an
# executable exiting with a status != 0.


TESTSDIR="/usr/libexec/rocm/$1"

if [ ! -e /dev/kfd ]
then
	echo "/dev/kfd not present, system either lacks AMD GPU or AMDGPU driver is not loaded."
	echo "Skipping tests."
	# Magic number to signal 'skipped'
	exit 77
elif [ "$(id -u)" != "0" ] && [ ! -r /dev/kfd ]
then
	echo "/dev/kfd present but no read permission."
	echo "Skipping tests."
	exit 77
elif [ -z "$1" ]
then
	echo "This script needs to be called with a directory name as an argument." >&2
	echo "Aborting." >&2
	exit 1
elif [ ! -d "$TESTSDIR" ]
then
	echo "The directory $TESTSDIR does not exist." >&2
	echo "Aborting." >&2
	exit 1
fi

# 16 = testbed failure
cd "$AUTOPKGTEST_TMP" || exit 16

# First, gather system info
sudo -n mount -t debugfs none /sys/kernel/debug || true
if sudo -n [ -d /sys/kernel/debug/dri ]
then
	for index in $(sudo -n ls /sys/kernel/debug/dri)
	do
		info="/sys/kernel/debug/dri/$index/amdgpu_firmware_info"
		if sudo -n [ -f "$info" ]
		then
			# shellcheck disable=SC2024   # we don't need privileged write
			sudo -n cat "$info" > "$AUTOPKGTEST_ARTIFACTS/amdgpu_firmware_info.$index"
		fi
	done
else
	echo "Could not read /sys/kernel/debug/dri" >> "$AUTOPKGTEST_ARTIFACTS/firmware.err"
fi
# shellcheck disable=SC2024   # we don't need privileged write
sudo -n dmesg > "$AUTOPKGTEST_ARTIFACTS/dmesg.before" || true

# Any individual failure is overall failure
EXITCODE=0
for TESTNAME in "$TESTSDIR"/*
do
	$TESTNAME || EXITCODE=1
done

# Tests might have generated new messages
# shellcheck disable=SC2024   # we don't need privileged write
sudo -n dmesg > "$AUTOPKGTEST_ARTIFACTS/dmesg.after" || true

exit $EXITCODE
